package com.example.mihaja.brique.Deplacable;

import android.content.Context;
import android.widget.FrameLayout;

/**
 * Created by mihaja on 29/03/2018.
 */
public class Plateau extends  Deplacable {
    public Plateau(Context c, int x, int y, int dx, int dy, int width, int height, FrameLayout conteneur)
    {
        this.conteneur=conteneur;
        setLongueur(height);
        setLargeur(width);
        setx(x);
        sety(y);
        setDx(dx);
        setDy(dy);
    }
    public void setX(int x)
    {
        Balle bol=cont.getB();
        if(bol.getx()+bol.getRayon()>x&&bol.getx()-bol.getRayon()<x+getLargeur()&&bol.gety()+bol.getRayon()>y&&bol.gety()-bol.getRayon()<y+getLongueur())
        {
            if(Math.signum(x-getx())>0)
            {
                if(x+getLargeur()+bol.getRayon()*2>+cont.getWidth())
                {
                    x=cont.getWidth()-bol.getRayon()*2-getLargeur();
                }
                bol.setx(x+getLargeur()+bol.getRayon());
            }
            if(Math.signum(x-getx())<0)
            {
                bol.setx(x-bol.getRayon());
            }
        }
        this.x=x;
    }
}
