package com.example.mihaja.brique.Deplacable;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.widget.FrameLayout;

import com.example.mihaja.brique.Affichage.EspaceJeu;

/**
 * Created by mihaja on 29/03/2018.
 */
public class Deplacable {
    int x;
    int y;
    EspaceJeu cont;
    public FrameLayout getConteneur() {
        return conteneur;
    }

    public void setConteneur(FrameLayout conteneur) {
        this.conteneur = conteneur;
    }

    FrameLayout conteneur;
    public int getx() {
        return x;
    }

    public void setx(int x) {
        if(this.x>conteneur.getWidth()-this.getLargeur()) {
            this.x=conteneur.getWidth()-this.getLargeur();
            this.setDx(-getDx());
            return;
        }
        if(this.x<0) {
            this.x=0;
            return;
        }
        this.x = x;

    }
    public int getcentrex()
    {
        return x+largeur/2;
    }
    public int getcentrey()
    {
        return y+longueur/2;
    }
    public int gety() {
        return y;
    }

    public void sety(int y) {
        this.y = y;
        if(this.y>=conteneur.getHeight()-this.getLongueur()||this.y<0)
            this.setDy(-getDy());
    }

    public int getDx() {
        return dx;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public EspaceJeu getCont() {
        return cont;
    }

    public void setCont(EspaceJeu cont) {
        this.cont = cont;
    }

    public int getDy() {
        return dy;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    int dx;
    int dy;

    public int getLargeur() {
        return largeur;
    }

    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }
    public Deplacable()
    {}
    public int getLongueur() {
        return longueur;
    }

    public void setLongueur(int longeur) {
        this.longueur = longeur;
    }
    Canvas canvas;
    int largeur,longueur;
    public Deplacable(Context c,int x,int y,int dx,int dy,int width,int height,FrameLayout conteneur)
    {
        this.conteneur=conteneur;
        setLongueur(height);
        setLargeur(width);
        setx(x);
        sety(y);
        setDx(dx);
        setDy(dy);
    }
    public void onDraw(Canvas canvas)
    {
        this.canvas=canvas;
        Paint p=new Paint();
        p.setColor(Color.BLUE);
        canvas.drawRect(x,y,getLargeur()+x,getLongueur()+y,p);
    }
    public void deplacer()
    {
        this.setx(this.getx()+this.getDx());
        //this.setActivated(true);
        //this.refreshDrawableState();
        //this.setActivated(false);
    }
}
