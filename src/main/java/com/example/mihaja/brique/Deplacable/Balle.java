package com.example.mihaja.brique.Deplacable;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.FrameLayout;

import com.example.mihaja.brique.Affichage.EspaceJeu;
import com.example.mihaja.brique.Lanceur.LanceBalle;

/**
 * Created by mihaja on 29/03/2018.
 */
public class Balle extends Deplacable {
    LanceBalle lb;
    Plateau plateau;

    public LanceBalle getLb() {
        return lb;
    }

    public void setLb(LanceBalle lb) {
        this.lb = lb;
    }

    public void setx(int x)
    {
        this.x = x;

        if(this.x>=cont.getWidth()-this.getRayon()) {
            this.x=cont.getWidth()-this.getRayon();
            this.setDx(-getDx());
            return;
        }
        if(this.x<this.getRayon()) {
            this.x=getRayon();
            this.setDx(-getDx());
            return;
        }
    }
    public void sety(int y)
    {

        this.y = y;
        if(cont!=null) {
            if (this.y >= cont.getHeight() - this.getRayon()) {
                this.y = cont.getHeight() - this.getRayon();
                dy = -dy;
                lb.interrupt();
                setLb(null);
                cont.getJoueur().setFail(true);
                cont.repaint();
                return;
            }
            if (this.y < this.getRayon()) {
                this.y = getRayon();
                this.dy = -dy;
                return;
            }
        }
    }
    public int getRayon() {
        return rayon;
    }

    public void setRayon(int rayon) {
        this.rayon = rayon;
    }

    int rayon;
    public Balle(Context c, int x, int y, int dx, int dy, int rayon, FrameLayout cont, EspaceJeu ej,Plateau plateau)
    {
       super();
        this.plateau=plateau;
        setConteneur(cont);
        this.cont=ej;
        setRayon(rayon);
        this.x=plateau.getx()+plateau.getLargeur()/2-rayon;
        this.y=plateau.gety()-getRayon();//+plateau.getLongueur();
        setDx(dx);
        setDy(dy);
        lb=new LanceBalle(this);
    }
    public void dessiner(Canvas canvas)
    {
        Paint p=new Paint();
        p.setColor(Color.parseColor("#6F30C851"));
        canvas.drawCircle(x,y,rayon,p);
    }
    public void deplacerBalle()
    {
        if(!lb.isAlive()&&!cont.getJoueur().isFail())
        lb.start();
    }
    public void deplacerB()
    {
        testerColision(plateau);
        if(cont.getLbrique().size()==0)
        {
            lb.interrupt();
            cont.getJoueur().setWin(true);
            cont.repaint();
            return;
        }
        for(int i=0;i<cont.getLbrique().size();i++)
        {
            Brique br=(Brique) cont.getLbrique().get(i);
            if(testerColision(br)) {
                cont.getJoueur().setScore(cont.getJoueur().getScore()+br.getPoint());
                cont.getScore().setText("Votre score="+cont.getJoueur().getScore());
                br.setPoint(0);
            }
        }
        this.setx(this.getx()+this.getDx());
        this.sety(this.gety()+this.getDy());
        cont.repaint();
    }
    public boolean testerColision(Deplacable atester)
    {
        if((this.x+dx>=atester.getx()&&x+dx<atester.getx()+atester.getLargeur())&&(y+dy+rayon>=atester.gety()&&y+dy-rayon<=atester.gety()+atester.getLongueur()))
        {
            this.dy=-dy;
            return true;
        }
        if((y+dy>=atester.gety()&&y+dy<=atester.gety()+atester.getLongueur())&&(x+dx+rayon>=atester.getx()&&x+dx-rayon<=atester.getx()+atester.getLargeur()))
        {
            this.dx=-dx;
            return  true;
        }
        if((-x-dx+atester.getx()<=rayon&&-x-dx+atester.getx()>=0)&&((-y-dy+atester.gety()>=0&&-y-dy+atester.gety()<=rayon)||(y+dy-atester.gety()-atester.getLongueur()>=0&&y+dy-atester.gety()-atester.getLongueur()<=rayon)))
        {
            if(Math.signum(x-atester.getcentrex())!=Math.signum(dx))
                dx=-dx;
            if(Math.signum(y-atester.getcentrey())!=Math.signum(dy))
                dy=-dy;
            return  true;
        }
        if((x+dx-atester.getx()-atester.getLargeur()<=rayon&&x+dx-atester.getx()-atester.getLargeur()>=0)&&((-y-dy+atester.gety()>=0&&-y-dy+atester.gety()<=rayon)||(y+dy-atester.gety()-atester.getLongueur()>=0&&y+dy-atester.gety()-atester.getLongueur()<=rayon)))
        {
            if(Math.signum(x-atester.getcentrex())!=Math.signum(dx))
                dx=-dx;
            if(Math.signum(y-atester.getcentrey())!=Math.signum(dy))
                dy=-dy;
            return true;
        }
        return  false;
    }
  /*  public void testerColision()
    {
        if((y>=plateau.gety()-getRayon())&&(x<=plateau.getx()+plateau.getLargeur()&&x>=plateau.getx())&&y-rayon<plateau.gety()+plateau.getLongueur())
        {
            y=plateau.gety()-getRayon();
            dy=-dy;
            return;
        }
        /*if(y-rayon<=plateau.gety()+plateau.getLongueur()&&y>plateau.gety()-getRayon()&&(x<=plateau.getx()+plateau.getLargeur()&&x>=plateau.getx()))
        {
            rayon=plateau.gety()+plateau.getLongueur();
            dy=-dy;
        }
        if((x==plateau.getx()-getRayon()&&(y<=plateau.gety()+plateau.getLongueur()&&y>=plateau.gety())))
        {
            dx=-dx;
            return;
        }
        if((x-rayon==plateau.getx()+plateau.getLargeur())&&y<=plateau.gety()+plateau.getLongueur()&&y>=plateau.gety())
        {
            dx=-dx;
            return;
        }
        if(x-plateau.getx()+plateau.getLargeur()<=rayon&&plateau.gety()-y<=rayon&&plateau.gety()-y>0)
        {
            dx=-dx;
            dy=-dy;
            return;
        }
        if(plateau.getx()+plateau.getLargeur()-x<=rayon&&y-plateau.gety()<=rayon&&y-plateau.gety()>0)
        {
            dx=-dx;
            dy=-dy;
        }
    }*/
}
