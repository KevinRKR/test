package com.example.mihaja.brique.Deplacable;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by mihaja on 03/04/2018.
 */
public class Brique extends Deplacable {
    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    int point;
    public void setx(int x)
    {
        this.x=x;
    }
    public void sety(int y)
    {
        this.y=y;
    }
    public Brique(int x,int y,int Long,int Larg,int point)
    {
        setx(x);
        sety(y);
        setLargeur(Larg);
        setLongueur(Long);
        setPoint(point);
    }
    public void dessiner(Canvas ca)
    {
        Paint p=new Paint();
        p.setColor(Color.parseColor("#CEE96D14"));
        if(point==0)
            return;
        if(point==1)
            p.setColor(Color.BLACK);
        if(point==2)
            p.setColor(Color.RED);
        ca.drawRect(x,y,x+getLargeur(),y+getLongueur(),p);
    }
}
