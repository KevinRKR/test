package com.example.mihaja.brique.Affichage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.example.mihaja.brique.Deplacable.Balle;
import com.example.mihaja.brique.Deplacable.Brique;
import com.example.mihaja.brique.Deplacable.Plateau;
import com.example.mihaja.brique.Joueur.Player;

import org.w3c.dom.Text;

import java.util.List;
import java.util.Vector;

/**
 * Created by mihaja on 29/03/2018.
 */
public class EspaceJeu extends SurfaceView implements SurfaceHolder.Callback {
    Paint p;
    Canvas canvas;
    Vector lbrique;
    public SurfaceHolder getMasurface() {
        return masurface;
    }

    public void setMasurface(SurfaceHolder masurface) {
        this.masurface = masurface;
    }

    public Balle getB() {
        return b;
    }

    public void setB(Balle b) {
        this.b = b;
    }

    SurfaceHolder masurface;
    Balle b;
    Plateau plateau;
    Player joueur;

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    Score score=new Score("Votre score=0");
    public Plateau getPlateau() {
        return plateau;
    }

    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    public Player getJoueur() {
        return joueur;
    }

    public void setJoueur(Player joueur) {
        this.joueur = joueur;
    }


    public EspaceJeu(Context c, Balle b, Plateau plateau, Player player)
    {
        super(c);
        this.joueur=player;

        this.b=b;
        this.plateau=plateau;
        b.setCont(this);
        plateau.setCont(this);
        p=new Paint();
        p.setColor(Color.RED);
        masurface=getHolder();
        masurface.addCallback(this);
    }

    public Vector getLbrique() {
        return lbrique;
    }

    public void setLbrique(Vector lbrique) {
        this.lbrique = lbrique;
    }

    public void deplacerBalle(){
        b.deplacerBalle();
    }
    public void PlateauGauche()
    {
        plateau.setx(plateau.getx()+plateau.getDx());
    }
    public void PlateauDroite()
    {
        plateau.setx(plateau.getx()-plateau.getDx());
    }
    public void repaint()
    {
        Canvas pCanvas=masurface.lockCanvas();
        draw(pCanvas);
        pCanvas.drawColor(Color.parseColor("#E0405CAF"));
        b.dessiner(pCanvas);
        if(getJoueur().isFail()||getJoueur().isWin())
        {
            Paint paint=new Paint();
            paint.setTextSize(80);
            paint.setColor(Color.RED);
            if(getJoueur().isWin())
            {
                pCanvas.drawText("Good Game",10,250,paint);
                masurface.unlockCanvasAndPost(pCanvas);
                return;
            }
            pCanvas.drawText("Game Over",10,250,paint);
        }
        dessinerbrique(pCanvas);
        score.dessiner(pCanvas);
        plateau.onDraw(pCanvas);
        masurface.unlockCanvasAndPost(pCanvas);
    }
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }


    @Override

    public void surfaceCreated(SurfaceHolder holder) {
        p.setColor(Color.parseColor("#FC2F62C7"));
        Canvas pCanvas=holder.lockCanvas();
        pCanvas.drawColor(Color.parseColor("#E04550AF"));
        dessinerbrique(pCanvas);
        score.dessiner(pCanvas);
        b.dessiner(pCanvas);
        plateau.onDraw(pCanvas);
        holder.unlockCanvasAndPost(pCanvas);

    }


    @Override

    public void surfaceDestroyed(SurfaceHolder holder) {


    }
    public void dessinerbrique(Canvas ca)
    {
        if(lbrique!=null)
        {
            for(int i=0;i<lbrique.size();i++)
            {
                Brique b=(Brique) lbrique.get(i);
                if(b.getPoint()==0)
                {
                    lbrique.remove(b);
                    i--;
                    continue;
                }
                b.dessiner(ca);
            }
            return;
        }
        lbrique=new Vector();
        int y=30;
        int larg=getWidth()/8                                                           ;
        int point=1;
        for(int i=0;i<3;i++)
        {
            int x=10;
            for(int j=0;j<6;j++) {
                Brique brique = new Brique(x,y,30,larg,point);
                lbrique.add(brique);
                brique.dessiner(ca);
                x+=larg+10;
            }
            point++;
            y+=40;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.i("event="+event.getAction(),"action="+MotionEvent.ACTION_BUTTON_PRESS);
        Log.i("event="+event.getAction(),"action="+MotionEvent.ACTION_MOVE);
        Log.i("event="+event.getAction(),"action="+MotionEvent.ACTION_BUTTON_RELEASE);
        Log.i("event="+event.getAction(),"action="+MotionEvent.ACTION_CANCEL);
        if(event.getAction()==MotionEvent.ACTION_DOWN)
        {
            Log.i("tafa ehhhh","okok");
            plateau.setX((int)event.getX());
            if(b.getLb()==null)
                return true;
            if(!b.getLb().isAlive())
            {
                b.setx(plateau.getx()+plateau.getLargeur()/2-b.getRayon());
                b.sety(plateau.gety()-b.getRayon());//+plateau.getLongueur();
                repaint();
            }
        }
        return true;
    }
}
