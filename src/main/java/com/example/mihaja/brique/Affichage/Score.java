package com.example.mihaja.brique.Affichage;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by mihaja on 04/04/2018.
 */
public class Score {
    String text="Score=0";

    public Score(String text) {
        this.text = text;
    }

    public String getText() {

        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    public void dessiner(Canvas can)
    {
        Paint p=new Paint();
        p.setColor(Color.BLACK);
        p.setTextSize(25);
        //p.setFontFeatureSettings("Arial");
        can.drawText(text,50,18,p);
    }
}
