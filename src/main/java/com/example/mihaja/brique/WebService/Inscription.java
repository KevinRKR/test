package com.example.mihaja.brique.WebService;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mihaja.brique.Acceuil;
import com.example.mihaja.brique.R;

/**
 * Created by ITU on 19/04/2018.
 */
public class Inscription extends AppCompatActivity {
    Acceuil precedent;

    public Acceuil getPrecedent() {
        return precedent;
    }

    public void setPrecedent(Acceuil precedent) {
        this.precedent = precedent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inscription);

    }
    public void Go(View v)
    {
        CallInscription sc=new CallInscription(((EditText)findViewById(R.id.editText)).getText().toString(),((EditText)findViewById(R.id.editText2)).getText().toString(),((EditText)findViewById(R.id.editText3)).getText().toString(),this);
        sc.setPb((ProgressBar) findViewById(R.id.progressBar));
        sc.setTv((TextView)findViewById(R.id.textView2));
        sc.execute();
        Log.i("Ok","ok"+sc.getLogin()+"ok");
        Log.i("Ok","ok"+sc.getMdp()+"ok");
    }
}
