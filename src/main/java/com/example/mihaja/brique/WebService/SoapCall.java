package com.example.mihaja.brique.WebService;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mihaja.brique.Acceuil;
import com.example.mihaja.brique.MainActivity;
import com.example.mihaja.brique.R;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by ITU on 13/04/2018.
 */
public class SoapCall extends AsyncTask<String,Object,String>{
    Object resultat;
    ProgressBar pb;
    public static  final String namespace="http://Kevs.org/";
    public static  final String url="http://192.168.43.187:18/WebSite2/Service.asmx?op=TesterLogin";

    public TextView getTv() {
        return tv;
    }

    public void setTv(TextView tv) {
        this.tv = tv;
    }

    public static  final String soapaction="http://Kevs.org/TesterLogin";
    public static  final String method="TesterLogin";
    String reponse;
    TextView tv;
    public Object getResultat() {
        return resultat;
    }

    public void setResultat(Object resultat) {
        this.resultat = resultat;
    }

    public ProgressBar getPb() {
        return pb;
    }

    public void setPb(ProgressBar pb) {
        this.pb = pb;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    String login;
    String mdp;
    Acceuil acceuil;
    public SoapCall(String ab,String bc,Acceuil acceuil)
    {
        login=ab.trim();
        mdp=bc.trim();
        this.acceuil=acceuil;
    }
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s!=null)
            resultat=s;
        //tv.setText(resultat+"Kevin");
        pb.setVisibility(View.INVISIBLE);
       /*if(s.compareTo("true")==0)
        {*/
            Intent intent=new Intent(acceuil, MainActivity.class);
        intent.putExtra("nom",login);
        acceuil.startActivity(intent);

            acceuil.finish();
       /* }
        else
        {
            tv.setText("Erreur de login ou mot de passe");
        }*/
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pb.setVisibility(View.VISIBLE);
    }

    @Override
    protected String doInBackground(String... params) {
        String reponse="";
        Log.i("Eto","");
        SoapObject request=new SoapObject(namespace,method);
        request.addProperty("login",login);
        request.addProperty("mdp",mdp);
        Log.i("Eto","");
        SoapSerializationEnvelope envelope=new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.dotNet=true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE transportSE=new HttpTransportSE(url);
        try{
            transportSE.call(soapaction,envelope);
            SoapPrimitive sp=(SoapPrimitive) envelope.getResponse();
            reponse=sp.toString();
        }
        catch (XmlPullParserException ex)
        {
            Log.d("Error 2=",ex.getLocalizedMessage()+"ok"+ex.getMessage());
        }
        catch (IOException ie)
        {
            Log.d("Error 3=",ie.toString()+"ok"+ie.getMessage());
        }
        // tv.setText(resultat+"Kevin");
        return reponse;
    }
}
