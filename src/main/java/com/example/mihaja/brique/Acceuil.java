package com.example.mihaja.brique;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mihaja.brique.WebService.Inscription;
import com.example.mihaja.brique.WebService.SoapCall;

/**
 * Created by ITU on 13/04/2018.
 */
public class Acceuil extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
    }
    public void go(View v)
    {
        SoapCall sc=new SoapCall(((EditText)findViewById(R.id.login)).getText().toString(),((EditText)findViewById(R.id.password)).getText().toString(),this);
        sc.setPb((ProgressBar) findViewById(R.id.pb));
        sc.setTv((TextView)findViewById(R.id.textView));
        sc.execute();
        Log.i("Ok","ok"+sc.getLogin()+"ok");
        Log.i("Ok","ok"+sc.getMdp()+"ok");

    }
    public void creer(View v)
    {
        Log.d("Inscription","tafa");
        Intent intent=new Intent(this,Inscription.class);
        startActivity(intent);
        this.finish();
    }

}
