package com.example.mihaja.brique.Lanceur;

import com.example.mihaja.brique.Deplacable.Balle;

/**
 * Created by mihaja on 29/03/2018.
 */
public class LanceBalle extends Thread {
    public Balle getB() {
        return b;
    }

    public void setB(Balle b) {
        this.b = b;
    }

    Balle b;
    boolean aspet=true;

    public boolean isAspet() {
        return aspet;
    }

    public void setAspet(boolean aspet) {
        this.aspet = aspet;
    }

    public LanceBalle(Balle b)
    {
        this.b=b;
    }
    public void run()
    {
        try{
            while(true)
            {
                if(aspet) {
                    b.deplacerB();
                    Thread.sleep(15);
                }
            }
        }
        catch (Exception e){}
    }
}
