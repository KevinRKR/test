package com.example.mihaja.brique;

import com.example.mihaja.brique.Affichage.EspaceJeu;

import java.io.Serializable;
import java.io.SerializablePermission;

/**
 * Created by mihaja on 20/04/2018.
 */
public class Teste implements Serializable {
    EspaceJeu ep;
    boolean test;

    public EspaceJeu getEp() {
        return ep;
    }

    public void setEp(EspaceJeu ep) {
        this.ep = ep;
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    public Teste(EspaceJeu ep, boolean test) {

        this.ep = ep;
        this.test = test;
    }
}
