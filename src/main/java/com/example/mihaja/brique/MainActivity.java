package com.example.mihaja.brique;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.IntegerRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.mihaja.brique.Affichage.EspaceJeu;
import com.example.mihaja.brique.Deplacable.Balle;
import com.example.mihaja.brique.Deplacable.Deplacable;
import com.example.mihaja.brique.Deplacable.Plateau;
import com.example.mihaja.brique.Joueur.Player;

import junit.framework.Test;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    boolean TesterPlay=false;
    EspaceJeu PlayArea;
    Integer i=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tv=(TextView)findViewById(R.id.textView4);
        if(this.getIntent().getStringExtra("nom")!=null)
        {
            tv.setVisibility(View.VISIBLE);
            tv.setText("Bienvenue   "+getIntent().getStringExtra("nom")+"!!!!!");
            getIntent().removeExtra("nom");
        }
        else{
            tv.setVisibility(View.INVISIBLE);
            TesterPlay=false;
            PlayArea=null;
            Log.d("tafa","dusk till dawn");
        }
    }
    public void deplacer(View v)
    {
        i++;
        if(!TesterPlay) {
            Log.d("tafa","swala");
            Player joueur=new Player(0,"Kevin");
            FrameLayout EspaceJeu = (FrameLayout) findViewById(R.id.EspaceJeu);
            EspaceJeu.setBackgroundColor(5);
            Plateau plateau=new Plateau(getApplicationContext(),50,EspaceJeu.getHeight()-50,10,0,100,30,EspaceJeu);
            Balle b=new Balle(null,10,0,1,-2,15,null,null,plateau);
            PlayArea=new EspaceJeu(getApplicationContext(),b,plateau,joueur);
            EspaceJeu.addView(PlayArea);
            TesterPlay=true;
        }
        else{
            PlayArea.deplacerBalle();
        }
    }
    public void PausePlay(View v)
    {
        Button source=(Button)v;
        try {
            if(this.PlayArea==null)
                return;
            if(this.PlayArea.getB().getLb()==null)
                return;
            if(!this.PlayArea.getB().getLb().isAlive())
                this.PlayArea.getB().getLb().start();
            if (source.getText().toString().compareToIgnoreCase("Pause") == 0) {
                PlayArea.getB().getLb().setAspet(false);
            }
            else
            {
                PlayArea.getB().getLb().setAspet(true);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void Restart(View v)
    {
        this.recreate();
    }

   /*@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("Kevin",new Teste(PlayArea,TesterPlay));
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        PlayArea=((Teste)savedInstanceState.getSerializable("Kevin")).getEp();
        TesterPlay=((Teste)savedInstanceState.getSerializable("Kevin")).isTest();
    }*/
}
