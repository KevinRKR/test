package com.example.mihaja.brique.Joueur;

/**
 * Created by mihaja on 03/04/2018.
 */
public class Player {
    int score;
    String nom;
    boolean fail=false;
    boolean win=false;

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public boolean isFail() {
        return fail;
    }

    public void setFail(boolean fail) {
        this.fail = fail;
    }

    public Player(int score, String nom) {
        this.score = score;
        this.nom = nom;
    }

    public int getScore() {
        return score;

    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
